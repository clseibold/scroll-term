# Scroll-Term

Licensed: MIT

Scroll-Term is a terminal client for the [Scroll](gemini://scrollprotocol.us.to), [Nex](gemini://nex.nightfall.city/nex/), and [Gemini](https://geminiprotocol.net) protocols.

Find out more information about the Scroll Protocol [on the Scroll Protocol homepage](scroll://scrollprotocol.us.to) or in the `ScrollSpec.scroll` file. The most up-to-date information will always be on the Scroll Protocol homepage.

## Install or Build

Install with golang:
```
go install gitlab.com/clseibold/scroll-term@latest
```

Or build locally with:
```
go build .
```

## Features
* Scroll, Gemini, and Nex support
* Basic navigation: go, home, back, forward, up, root, refresh, download, and url (to see current location)
* History, Outline, and Links
* Scroll Protocol: Metadata and Abstracts
* Pretty Print: documents are printed to terminal wrapped to a max-width, and centered on the terminal (if applicable)
* User Input for Gemini and Scroll
* Documents: gemtext, scrolltext, nex, and markdown
* Text Streaming: all text documents are converted and printed as they are streamed in.
* Audio Streaming: mp3, ogg vorbis, wave, and flac
* Video files piped into mpv
* Unicode word-wrapping
* Strong, emphasis, and monospace via bold, italics, and dim (currently VT-100 only)

## Planned Features
* Support Spartan Protocol
* Configuration directory to store config files and user certs
* Bookmarks page, and ability to add bookmarks
* Get proper Downloads directory
* Portable mode - downloads directory and configuration are next to executable
* Spartan/Titan Upload (text/editor vs. file upload)
* Support Gopher?
* User Certificates
* TOFU Store and Validation handling.
* Scheme and file handlers:
  - Set in software
  - Or use Operating System's stuff
* Paging Mode
* TUI Mode
* Tour Mode
* Option to print page(s) with line numbers
* Search Engine setting
* Proxy setting?
* Mark links - like bookmarks, but only for current process session.
* Shell - pipe output of file to a process
* Help system
* Local file browsing
* Search Page and Links

## Images
Example of getting the Abstract of a page:
![imgs/abstract_example.png](imgs/abstract_example.png)

Example of getting a page:
![imgs/page_example.png](imgs/page_example.png)
