package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"
	"unicode"

	"github.com/fatih/color"
	"github.com/rivo/uniseg"
)

// Does word-wrapping based on where grapheme clusters are allowed to break.
func UnicodeWordWrap(str string, width int) string {
	g := uniseg.NewGraphemes(str)
	currentWidth := 0

	var result strings.Builder
	var nonBreaking strings.Builder
	var nonBreaking_width int

	// Go through each grapheme cluster
	for g.Next() {
		// If adding width to currentWidth overflows, then we must add a line break to result (before the current nonBlocking section)
		if g.Width()+currentWidth > width {
			// Add newline to result
			result.WriteRune('\n')
			currentWidth = nonBreaking_width
		}

		currentWidth += g.Width()
		if g.LineBreak() == uniseg.LineCanBreak {
			// Move nonBreaking to result and add current cluster to result
			result.WriteString(nonBreaking.String())
			nonBreaking_width = 0
			nonBreaking.Reset()
			result.WriteString(g.Str())
		} else if g.LineBreak() == uniseg.LineMustBreak {
			result.WriteString(nonBreaking.String())
			nonBreaking_width = 0
			nonBreaking.Reset()
			result.WriteString(g.Str())
		} else if g.LineBreak() == uniseg.LineDontBreak {
			nonBreaking.WriteString(g.Str())
			nonBreaking_width += g.Width()
		}
	}

	if g.LineBreak() == uniseg.LineMustBreak {
		result.WriteString(nonBreaking.String())
		//result.WriteRune('\n')
	}

	return result.String()
}

func UnicodeGraphemeWrap(str string, width int) string {
	return ""
}

type Link struct {
	Id           int
	Text         string
	Url          string
	Relation     int // Can be -1, 0, or 1
	Tag          string
	SpartanInput bool
	CrossHost    bool
	WebLink      bool
}

type Heading struct {
	Level int
	Text  string
}

func PrintText(context *Context, reader io.Reader, pageData *bytes.Buffer, indent int, showLineNumbers bool) {
	//wordWrapper := wordwrap.Wrapper(context.maxwidth, false)
	//wordWrapper_withlinenumbers := wordwrap.Wrapper(context.maxwidth-5, false)
	indentationString := strings.Repeat(" ", indent)

	scanner := bufio.NewScanner(reader)
	linenumber := 1
	for scanner.Scan() {
		scanned_text := scanner.Text()
		if pageData != nil {
			pageData.Write([]byte(scanned_text + "\r\n"))
		}

		line := strings.TrimRight(scanned_text, "\r\n")
		if !showLineNumbers {
			multiline := UnicodeWordWrap(line, context.maxwidth)
			//multiline := wordWrapper(line)
			lines := strings.Split(multiline, "\n")
			for _, line := range lines {
				fmt.Printf("%s%s\n", indentationString, line)
				linenumber++
			}
		} else {
			multiline := UnicodeWordWrap(line, context.maxwidth-5)
			//multiline := wordWrapper_withlinenumbers(line)
			lines := strings.Split(multiline, "\n")
			for _, line := range lines {
				fmt.Printf("%s%5s%s\n", indentationString, strconv.Itoa(linenumber), line)
				linenumber++
			}
		}
	}
}

func PrintNex(context *Context, currentURL *url.URL, reader io.Reader, links *[]Link, pageData *bytes.Buffer, indent int, showLineNumbers bool) {
	if links == nil {
		links = &[]Link{}
	}

	// wordWrapper := wordwrap.Wrapper(context.maxwidth, false)
	// wordWrapper_withlinenumbers := wordwrap.Wrapper(context.maxwidth-5, false)
	indentationString := strings.Repeat(" ", indent)

	scanner := bufio.NewScanner(reader)
	linenumber := 1
	for scanner.Scan() {
		scanned_text := scanner.Text()
		if pageData != nil {
			pageData.Write([]byte(scanned_text + "\r\n"))
		}

		line := strings.TrimRight(scanned_text, "\r\n")
		if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, hasTitle := CutAny(line, " \t")

			link = strings.TrimSpace(link)
			link_without_fragment, _, _ := strings.Cut(link, "#")

			URL, err := url.Parse(link_without_fragment)
			if err != nil {
				continue
			}
			crossHost := false
			if URL.Scheme != "" && URL.Scheme != currentURL.Scheme && URL.Hostname() != currentURL.Hostname() {
				crossHost = true
			}

			title = strings.TrimSpace(title)
			tag := ""
			relation := 0
			if hasTitle && strings.HasSuffix(title, "]") {
				title, tag, _ = strings.Cut(title, "[")
				tag = strings.TrimSuffix(strings.TrimPrefix(tag, "["), "]")
				if strings.HasPrefix(tag, "-") {
					relation = -1
					tag = strings.TrimPrefix(tag, "-")
				} else if strings.HasPrefix(tag, "+") {
					relation = 1
					tag = strings.TrimPrefix(tag, "+")
				}
			}

			URL, _ = currentURL.Parse(link_without_fragment)
			linkType := Link{len(*links), title, URL.String(), relation, tag, false, crossHost, URL.Scheme == "http" || URL.Scheme == "https"}
			*links = append(*links, linkType)

			if linkType.WebLink {
				fmt.Print(color.RedString("%s[%d]", indentationString, linkType.Id))
			} else if crossHost {
				fmt.Print(color.YellowString("%s[%d]", indentationString, linkType.Id))
			} else {
				fmt.Print(color.CyanString("%s[%d]", indentationString, linkType.Id))
			}
			if hasTitle {
				fmt.Printf(" => %s %s", link_without_fragment, linkType.Text)
			} else {
				fmt.Printf(" => %s", link_without_fragment)
			}
			/*if URL.RawQuery != "" {
				query, _ := url.QueryUnescape(URL.RawQuery)
				fmt.Print(" (Query: '")
				for i, r := range query {
					if i == 20 {
						break
					}
					fmt.Print(string(r))
				}
				if 10 < len(query) {
					fmt.Print("...")
				}
				fmt.Print("')")
			}*/
			fmt.Printf("\n")
			linenumber++
		} else {
			if !showLineNumbers {
				state := TextParsingState{}
				multiline := UnicodeWordWrap(line, context.maxwidth)
				//multiline := wordWrapper(line)
				lines := strings.Split(multiline, "\n")
				for _, line := range lines {
					fmt.Printf("%s", indentationString)
					state.print_markdown(bufio.NewReader(strings.NewReader(line)), true)
					fmt.Printf("\n")
					linenumber++
				}
			} else {
				state := TextParsingState{}
				multiline := UnicodeWordWrap(line, context.maxwidth-5)
				//multiline := wordWrapper_withlinenumbers(line)
				lines := strings.Split(multiline, "\n")
				for _, line := range lines {
					fmt.Printf("%s%5s", indentationString, strconv.Itoa(linenumber))
					state.print_markdown(bufio.NewReader(strings.NewReader(line)), true)
					fmt.Printf("\n")
					linenumber++
				}
			}
		}
	}
}

// PrintGemini handles Markdown, Gemini, and Scroll
func PrintGemini(context *Context, currentURL *url.URL, reader io.Reader, headings *[]Heading, links *[]Link, pageData *bytes.Buffer, indent int) (string, bool) {
	if links == nil {
		links = &[]Link{}
	}

	// wordWrapper := wordwrap.Wrapper(context.maxwidth, false)
	// wordWrapper_quotes := wordwrap.Wrapper(context.maxwidth-2, false)
	//wordWrapper_preformat := wordwrap.Wrapper(context.maxwidth, true)
	// wordWrapper_heading1 := wordWrapper_quotes
	// wordWrapper_heading2 := wordwrap.Wrapper(context.maxwidth-3, false)
	// wordWrapper_heading3 := wordwrap.Wrapper(context.maxwidth-4, false)
	// wordWrapper_heading4 := wordwrap.Wrapper(context.maxwidth-5, false)
	indentationString := strings.Repeat(" ", indent)

	title := ""
	lastTitleLevel := 5
	isFeed := false

	scanner := bufio.NewScanner(reader)
	inPreformat := false
	for scanner.Scan() {
		scanned_text := scanner.Text()
		if pageData != nil {
			pageData.Write([]byte(scanned_text + "\r\n"))
		}

		line := strings.TrimRight(scanned_text, "\r\n")
		if title == "" && strings.TrimSpace(line) != "" {
			if ContainsLetterRunes(line) {
				// Assume for nex documents that the first non-blank line is the title
				title = strings.TrimSpace(line)
			}
		}
		if inPreformat {
			if strings.HasPrefix(line, "```") {
				fmt.Printf("%s```\n", indentationString)
				inPreformat = false
				fmt.Printf("\x1B[22m") // Unset dim
				continue
			}
			//multiline := wordWrapper_preformat(line)
			//lines := strings.Split(multiline, "\n")
			//for _, line := range lines {
			fmt.Printf("%s%s\n", indentationString, line)
			//}
			continue
		}

		if strings.HasPrefix(line, "```") {
			fmt.Printf("\x1B[2m") // Set dim
			fmt.Printf("%s```\n", indentationString)
			inPreformat = !inPreformat
		} else if line == "---" {
			fmt.Printf("%s\n", strings.Repeat("-", indent))
		} else if strings.HasPrefix(line, "####") {
			text := strings.TrimSpace(strings.TrimPrefix(line, "####"))
			if title == "" || lastTitleLevel > 4 {
				title = text
				lastTitleLevel = 4
			}
			if headings != nil {
				*headings = append(*headings, Heading{4, text})
			}

			multiline := UnicodeWordWrap(text, context.maxwidth-5)
			//multiline := wordWrapper_heading4(text)
			lines := strings.Split(multiline, "\n")
			for i, line := range lines {
				if i == 0 {
					color.Cyan("%s#### %s\n", indentationString, line)
				} else {
					color.Cyan("%s     %s\n", indentationString, line)
				}
			}
		} else if strings.HasPrefix(line, "###") {
			text := strings.TrimSpace(strings.TrimPrefix(line, "###"))
			if title == "" || lastTitleLevel > 3 {
				title = text
				lastTitleLevel = 3
			}
			if headings != nil {
				*headings = append(*headings, Heading{3, text})
			}

			multiline := UnicodeWordWrap(text, context.maxwidth-4)
			//multiline := wordWrapper_heading3(text)
			lines := strings.Split(multiline, "\n")
			for i, line := range lines {
				if i == 0 {
					color.Cyan("%s### %s\n", indentationString, line)
				} else {
					color.Cyan("%s    %s\n", indentationString, line)
				}
			}
		} else if strings.HasPrefix(line, "##") {
			text := strings.TrimSpace(strings.TrimPrefix(line, "##"))
			if title == "" || lastTitleLevel > 2 {
				title = text
				lastTitleLevel = 2
			}
			if headings != nil {
				*headings = append(*headings, Heading{2, text})
			}

			multiline := UnicodeWordWrap(text, context.maxwidth-3)
			//multiline := wordWrapper_heading2(text)
			lines := strings.Split(multiline, "\n")
			for i, line := range lines {
				if i == 0 {
					color.Cyan("%s## %s\n", indentationString, line)
				} else {
					color.Cyan("%s   %s\n", indentationString, line)
				}
			}
		} else if strings.HasPrefix(line, "#") {
			text := strings.TrimSpace(strings.TrimPrefix(line, "#"))
			if title == "" || lastTitleLevel > 1 {
				title = text
				lastTitleLevel = 1
			}
			if headings != nil {
				*headings = append(*headings, Heading{1, text})
			}

			multiline := UnicodeWordWrap(text, context.maxwidth-2)
			//multiline := wordWrapper_heading1(text)
			lines := strings.Split(multiline, "\n")
			for i, line := range lines {
				if i == 0 {
					color.Cyan("%s# %s\n", indentationString, line)
				} else {
					color.Cyan("%s  %s\n", indentationString, line)
				}
			}
		} else if strings.HasPrefix(line, "=:") {
			// Input Link: Don't put in urls to crawl
			line = strings.TrimSpace(strings.TrimPrefix(line, "=:"))
			link, title, hasTitle := CutAny(line, " \t")

			link = strings.TrimSpace(link)
			link_without_fragment, _, _ := strings.Cut(link, "#")

			URL, err := url.Parse(link_without_fragment)
			if err != nil {
				continue
			}
			crossHost := false
			if URL.Scheme != "" && URL.Scheme != currentURL.Scheme && URL.Hostname() != currentURL.Hostname() {
				crossHost = true
			}

			title = strings.TrimSpace(title)
			tag := ""
			relation := 0
			if hasTitle && strings.HasSuffix(title, "]") {
				title, tag, _ = strings.Cut(title, "[")
				tag = strings.TrimSuffix(strings.TrimPrefix(tag, "["), "]")
				if strings.HasPrefix(tag, "-") {
					relation = -1
					tag = strings.TrimPrefix(tag, "-")
				} else if strings.HasPrefix(tag, "+") {
					relation = 1
					tag = strings.TrimPrefix(tag, "+")
				}
			}

			URL, _ = currentURL.Parse(link_without_fragment)
			linkType := Link{len(*links), title, URL.String(), relation, tag, true, crossHost, URL.Scheme == "http" || URL.Scheme == "https"}
			*links = append(*links, linkType)

			if linkType.WebLink {
				fmt.Print(color.RedString("%s[%d Input]", indentationString, linkType.Id))
			} else if crossHost {
				fmt.Print(color.YellowString("%s[%d Input]", indentationString, linkType.Id))
			} else {
				fmt.Print(color.CyanString("%s[%d Input]", indentationString, linkType.Id))
			}
			if hasTitle {
				fmt.Printf(" %s", linkType.Text)
			} else {
				fmt.Printf(" %s", linkType.Url)
			}
			if URL.RawQuery != "" {
				query, _ := url.QueryUnescape(URL.RawQuery)
				fmt.Print(" (Query: '")
				for i, r := range query {
					if i == 20 {
						break
					}
					fmt.Print(string(r))
				}
				if 10 < len(query) {
					fmt.Print("...")
				}
				fmt.Print("')")
			}
			fmt.Printf("\n")

			if isTimeDate(title) {
				isFeed = true
			}
		} else if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, hasTitle := CutAny(line, " \t")

			link = strings.TrimSpace(link)
			link_without_fragment, _, _ := strings.Cut(link, "#")

			URL, err := url.Parse(link_without_fragment)
			if err != nil {
				continue
			}
			crossHost := false
			if URL.Scheme != "" && URL.Scheme != currentURL.Scheme && URL.Hostname() != currentURL.Hostname() {
				crossHost = true
			}

			title = strings.TrimSpace(title)
			tag := ""
			relation := 0
			if hasTitle && strings.HasSuffix(title, "]") {
				title, tag, _ = strings.Cut(title, "[")
				tag = strings.TrimSuffix(strings.TrimPrefix(tag, "["), "]")
				if strings.HasPrefix(tag, "-") {
					relation = -1
					tag = strings.TrimPrefix(tag, "-")
				} else if strings.HasPrefix(tag, "+") {
					relation = 1
					tag = strings.TrimPrefix(tag, "+")
				}
			}

			URL, _ = currentURL.Parse(link_without_fragment)
			linkType := Link{len(*links), title, URL.String(), relation, tag, false, crossHost, URL.Scheme == "http" || URL.Scheme == "https"}
			*links = append(*links, linkType)

			if linkType.WebLink {
				fmt.Print(color.RedString("%s[%d]", indentationString, linkType.Id))
			} else if crossHost {
				fmt.Print(color.YellowString("%s[%d]", indentationString, linkType.Id))
			} else {
				fmt.Print(color.CyanString("%s[%d]", indentationString, linkType.Id))
			}
			if hasTitle {
				fmt.Printf(" %s", linkType.Text)
			} else {
				fmt.Printf(" %s", linkType.Url)
			}
			if URL.RawQuery != "" {
				query, _ := url.QueryUnescape(URL.RawQuery)
				fmt.Print(" (Query: '")
				for i, r := range query {
					if i == 20 {
						break
					}
					fmt.Print(string(r))
				}
				if 10 < len(query) {
					fmt.Print("...")
				}
				fmt.Print("')")
			}
			fmt.Printf("\n")

			if isTimeDate(title) {
				isFeed = true
			}
		} else if strings.HasPrefix(line, ">") {
			parsingState := TextParsingState{}
			multiline := UnicodeWordWrap(strings.TrimLeft(strings.TrimPrefix(line, ">"), " \t"), context.maxwidth-2)
			//multiline := wordWrapper_quotes(strings.TrimLeft(strings.TrimPrefix(line, ">"), " \t"))
			lines := strings.Split(multiline, "\n")
			for _, line := range lines {
				fmt.Printf("%s> ", indentationString)
				parsingState.print_markdown(bufio.NewReader(strings.NewReader(line)), true)
				fmt.Printf("\n")
			}
			// Make sure everything is reset
			fmt.Print("\u001B[m")
		} else if strings.HasPrefix(line, "* ") || strings.HasPrefix(line, "** ") || strings.HasPrefix(line, "*** ") || strings.HasPrefix(line, "**** ") {
			level := strings.IndexFunc(line, func(r rune) bool {
				return r != '*'
			})
			parsingState := TextParsingState{}
			//wordWrapper = wordwrap.Wrapper(context.maxwidth-2-((level-1)*2), false)
			multiline := UnicodeWordWrap(strings.TrimLeft(line[level:], " \t"), context.maxwidth-2-((level-1)*2))
			// multiline := wordWrapper(strings.TrimLeft(line[level:], " \t"))
			lines := strings.Split(multiline, "\n")
			for i, line := range lines {
				if i == 0 {
					fmt.Printf("%s%s* ", indentationString, strings.Repeat("  ", level-1))
				} else {
					fmt.Printf("%s%s  ", indentationString, strings.Repeat("  ", level-1))
				}
				parsingState.print_markdown(bufio.NewReader(strings.NewReader(line)), true)
				fmt.Printf("\n")
			}
			// Make sure everything is reset
			fmt.Print("\u001B[m")
		} else {
			parsingState := TextParsingState{}
			//multiline := wordWrapper(line)
			multiline := UnicodeWordWrap(line, context.maxwidth)
			lines := strings.Split(multiline, "\n")
			for _, line := range lines {
				fmt.Printf("%s", indentationString)
				parsingState.print_markdown(bufio.NewReader(strings.NewReader(line)), true)
				fmt.Printf("\n")
			}
			// Make sure everything is reset
			fmt.Print("\u001B[m")
		}
	}

	return title, isFeed
}

type TextParsingState struct {
	previousRune rune
	inStrong     bool
	inEmphasis   bool
	inMonospace  bool
}

// Emphasis - one asterisk or one underscore
// Strong - two asterisks (or two underscores)
// Monospace - one backtick (`)
// Nesting is not supported. Pass in a bool to not reset the state on EOF
func (state *TextParsingState) print_markdown(reader *bufio.Reader, noEOFReset bool) {
	isBoundary := func(r rune) bool {
		return unicode.IsSpace(r) || unicode.IsPunct(r)
	}
	state.previousRune = '\n'

	for {
		r, _, err := reader.ReadRune()
		if err != nil {
			// EOF - reset everything, since this should be the end of the paragraph
			if !noEOFReset {
				fmt.Printf("\x1B[m")
				state.inStrong = false
				state.inEmphasis = false
				state.inMonospace = false
			}
			return
		}

		if r == '`' || (!state.inMonospace && (r == '*' || r == '_')) {
			toggleRune := r
			toggle := string(r)
			unread := true

			// Get the next rune
			r, _, err = reader.ReadRune()
			if err != nil {
				// Set rune to new line, so it registers as a whitespace.
				r = '\n'
				unread = false
			}

			// If r is an asterisk, we require two for the toggle, so read the next rune. Otherwise, if just one asterisk,
			// continue on as it's an italic.
			if toggleRune == '*' && r == '*' {
				toggle = "**"
				r, _, err = reader.ReadRune()
				if err != nil {
					// Set rune to new line, so it registers as a whitespace.
					r = '\n'
					unread = false
				}
			} else if toggleRune == '_' && r == '_' {
				toggle = "__"
				r, _, err = reader.ReadRune()
				if err != nil {
					// Set rune to new line, so it registers as a whitespace.
					r = '\n'
					unread = false
				}
			}

			if (isBoundary(state.previousRune) && !unicode.IsSpace(r) && r != toggleRune) || (!unicode.IsSpace(state.previousRune) && state.previousRune != toggleRune && isBoundary(r)) {
				switch toggleRune {
				case '`':
					if state.inMonospace {
						// Reset
						state.inMonospace = false
						fmt.Printf("\x1B[22m")
						// Since 22m disables bold *and* dim, set bold again if necessary
						if state.inStrong {
							fmt.Printf("\x1B[1m")
						}
					} else {
						// Set
						state.inMonospace = true
						fmt.Printf("\x1B[2m")
					}
				case '*':
					if toggle == "**" { // Bold
						if state.inStrong {
							// Reset
							state.inStrong = false
							// 22m disables bold *and* dim. However, we cannot use strong inside monospace toggles, so this doesn't matter.
							fmt.Printf("\x1B[22m")
						} else {
							// Set
							state.inStrong = true
							fmt.Printf("\x1B[1m")
						}
					} else { // Italics
						if state.inEmphasis {
							// Reset
							state.inEmphasis = false
							fmt.Printf("\x1B[23m")
						} else {
							// Set
							state.inEmphasis = true
							fmt.Printf("\x1B[3m") // Replace this with 4m for underline in terminals that don't support emphasis.
						}
					}
				case '_':
					if toggle == "__" { // Bold
						if state.inStrong {
							// Reset
							state.inStrong = false
							// 22m disables bold *and* dim. However, we cannot use strong inside monospace toggles, so this doesn't matter.
							fmt.Printf("\x1B[22m")
						} else {
							// Set
							state.inStrong = true
							fmt.Printf("\x1B[1m")
						}
					} else { // Italics
						if state.inEmphasis {
							// Reset
							state.inEmphasis = false
							fmt.Printf("\x1B[23m")
						} else {
							// Set
							state.inEmphasis = true
							fmt.Printf("\x1B[3m") // Replace this with 4m for underline in terminals that don't support emphasis.
						}
					}
				}
				_ = reader.UnreadRune()
				if unread {
					state.previousRune = toggleRune
				}
			} else {
				// Not a toggle, print the toggle and unread the rune
				fmt.Printf("%s", toggle)
				_ = reader.UnreadRune()
				state.previousRune = toggleRune
			}
		} else {
			fmt.Printf("%c", r)
			state.previousRune = r
		}
	}
}

// Emphasis - one underscore
// Strong - two asterisks
// Monospace - one backtick (`)
// Pass in a bool to not reset the state on EOF
func (state *TextParsingState) print_scroll(reader *bufio.Reader, noEOFReset bool) {
	isWhitespace := unicode.IsSpace
	state.previousRune = '\n'

	for {
		r, _, err := reader.ReadRune()
		if err != nil {
			// EOF - reset everything, since this should be the end of the paragraph
			if !noEOFReset {
				fmt.Printf("\x1B[m")
				state.inStrong = false
				state.inMonospace = false
				state.inEmphasis = false
			}
			return
		}

		if r == '`' || (!state.inMonospace && (r == '*' || r == '_')) {
			toggleRune := r
			toggle := string(r)
			unread := true

			// Get the next rune
			r, _, err = reader.ReadRune()
			if err != nil {
				// Set rune to space, so it registers as a whitespace.
				r = ' '
				unread = false
			}

			// If r is an asterisk, we require two for the toggle, so read the next rune. Otherwise, if just one asterisk,
			// print the runes and continue
			if toggleRune == '*' && r == '*' {
				toggle = "**"
				r, _, err = reader.ReadRune()
				if err != nil {
					// Set rune to space, so it registers as a whitespace.
					r = ' '
					unread = false
				}
			} else if toggleRune == '*' {
				fmt.Printf("*")
				_ = reader.UnreadRune()
				state.previousRune = '*'
				continue
			}

			if (!isWhitespace(r) && r != toggleRune) || (!isWhitespace(state.previousRune) && state.previousRune != toggleRune) {
				switch toggleRune {
				case '`':
					if state.inMonospace {
						// Reset
						state.inMonospace = false
						fmt.Printf("\x1B[22m")
						// Since 22m disables bold *and* dim, set bold again if necessary
						if state.inStrong {
							fmt.Printf("\x1B[1m")
						}
					} else {
						// Set
						state.inMonospace = true
						fmt.Printf("\x1B[2m")
					}
				case '*':
					if state.inStrong {
						// Reset
						state.inStrong = false
						// 22m disables bold *and* dim. However, we cannot use strong inside monospace toggles, so this doesn't matter.
						fmt.Printf("\x1B[22m")
					} else {
						// Set
						state.inStrong = true
						fmt.Printf("\x1B[1m")
					}
				case '_':
					if state.inEmphasis {
						// Reset
						state.inEmphasis = false
						fmt.Printf("\x1B[23m")
					} else {
						// Set
						state.inEmphasis = true
						fmt.Printf("\x1B[3m") // Replace this with 4m for underline in terminals that don't support emphasis.
					}
				}
				_ = reader.UnreadRune()
				if unread {
					state.previousRune = toggleRune
				}
			} else {
				// Not a toggle, print the toggle and unread the rune
				fmt.Printf("%s", toggle)
				_ = reader.UnreadRune()
				state.previousRune = toggleRune
			}
		} else {
			fmt.Printf("%c", r)
			state.previousRune = r
		}
	}
}

// Emphasis - one underscore
// Strong - one asterisk
// Monospace - one backtick (`)
// Pass in a bool to not reset the state on EOF
func (state *TextParsingState) print_asciidoc(reader *bufio.Reader, noEOFReset bool) {
	isWhitespace := unicode.IsSpace
	state.previousRune = '\n'

	for {
		r, _, err := reader.ReadRune()
		if err != nil {
			// EOF - reset everything, since this should be the end of the paragraph
			if !noEOFReset {
				fmt.Printf("\x1B[m")
				state.inStrong = false
				state.inMonospace = false
				state.inEmphasis = false
			}
			return
		}

		if r == '`' || (!state.inMonospace && (r == '*' || r == '_')) {
			toggleRune := r
			toggle := string(r)
			unread := true

			// Get the next rune
			r, _, err = reader.ReadRune()
			if err != nil {
				// Set rune to space, so it registers as a whitespace.
				r = ' '
				unread = false
			}

			if (!isWhitespace(r) && r != toggleRune) || (!isWhitespace(state.previousRune) && state.previousRune != toggleRune) {
				switch toggleRune {
				case '`':
					if state.inMonospace {
						// Reset
						state.inMonospace = false
						fmt.Printf("\x1B[22m")
						// Since 22m disables bold *and* dim, set bold again if necessary
						if state.inStrong {
							fmt.Printf("\x1B[1m")
						}
					} else {
						// Set
						state.inMonospace = true
						fmt.Printf("\x1B[2m")
					}
				case '*':
					if state.inStrong {
						// Reset
						state.inStrong = false
						// 22m disables bold *and* dim. However, we cannot use strong inside monospace toggles, so this doesn't matter.
						fmt.Printf("\x1B[22m")
					} else {
						// Set
						state.inStrong = true
						fmt.Printf("\x1B[1m")
					}
				case '_':
					if state.inEmphasis {
						// Reset
						state.inEmphasis = false
						fmt.Printf("\x1B[23m")
					} else {
						// Set
						state.inEmphasis = true
						fmt.Printf("\x1B[3m") // Replace this with 4m for underline in terminals that don't support emphasis.
					}
				}
				_ = reader.UnreadRune()
				if unread {
					state.previousRune = toggleRune
				}
			} else {
				// Not a toggle, print the toggle and unread the rune
				fmt.Printf("%s", toggle)
				_ = reader.UnreadRune()
				state.previousRune = toggleRune
			}
		} else {
			fmt.Printf("%c", r)
			state.previousRune = r
		}
	}
}
