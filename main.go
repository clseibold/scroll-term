package main

import (
	"bufio"
	"bytes"
	"container/ring"
	"fmt"
	"io"
	"mime"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	_ "time/tzdata"

	"github.com/clseibold/go-gemini"
	"github.com/eidolon/wordwrap"
	"github.com/fatih/color"
	"github.com/gabriel-vasile/mimetype"
	"github.com/gopxl/beep/flac"
	"github.com/gopxl/beep/mp3"
	"github.com/gopxl/beep/speaker"
	"github.com/gopxl/beep/vorbis"
	"github.com/gopxl/beep/wav"
	"github.com/pkg/browser"
	nex "gitlab.com/clseibold/gonex/nex_client"
	scroll "gitlab.com/clseibold/scroll-term/scroll_client"
	"golang.org/x/term"
)

const Version = "0.1"
const DateTime_Kitchen = "2006-01-02 3:04PM"

type Certificate []byte

type Context struct {
	scroll_client     *scroll.Client
	gemini_client     *gemini.Client
	nex_client        *nex.Client
	languages         []string
	currentURL        *url.URL
	currentLoadedCert Certificate

	supportMPV bool
	pageCache  *ring.Ring

	homepage string
	maxwidth int // Character width of all content when printed out to the terminal
	indent   int
}

type PageType int

const PageType_URL PageType = 0
const PageType_History PageType = 1
const PageType_Bookmarks PageType = 2

type PageCache struct {
	InForwards       bool
	PageType         PageType
	url              *url.URL
	Mediatype        string
	Params           map[string]string
	Author           string
	PublishDate      time.Time // Should be in UTC
	ModificationDate time.Time // Should be in UTC
	Title            string
	Abstract         string
	PageData         []byte // TODO: Convert to UTF-8 if necessary based on "charset" mimetype param.

	Headings []Heading
	Links    []Link
}

// TODO List:
// * Sanitize all nex/gemini/scroll and most plaintext documents to remove certain terminal escape codes
// * Support Spartan and Nex
// * Configuration directory to store config files and user certs
// * Bookmarks page, and ability to add bookmarks
// * Get proper Downloads directory
// * Portable mode - downloads directory and configuration are next to executable
// * Spartan/Titan Upload (text/editor vs. file upload)
// * Support Gopher?
// * User Certificates
// * TOFU Store and Validation handling.
// * Scheme and file handlers:
//   - Set in software
//   - Or use Operating System's stuff
// * Paging Mode
// * TUI Mode
// * Tour Mode
// * Option to print page(s) with line numbers
// * Search Engine setting
// * Proxy setting?
// * Mark links - like bookmarks, but only for current process session.
// * Shell - pipe output of file to a process
// * Help system
// * Local file browsing

// * Search Page
// * Search Link(s)

// * Settings options to Implement:
//   - TLS Mode (tofu vs. CA)
//   - Default read timeout?
//   - ipv6
//   - gopher/gemini proxy
//   - Auto-follow redirects
//   - Max-content-width (default to 85)
//   - Handlers
//   - Default to TUI/Paging Modes
//   - Always print with line numbers
//   - Search Engine URL (allow for multiples?)
//   - Link Rewriting (allows to rewrite https links to scroll/gemini equivalents, useful for YT proxies and other such things)

func main() {
	reader := bufio.NewReader(os.Stdin)

	language := ""
	fmt.Print("What language do you want to use? ('en') ")
	var err error
	language, err = reader.ReadString('\n')
	if err != nil || language == "" {
		language = "en"
	}

	termwidth, _, err := term.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		//termwidth = 80 * 3
	}
	//maxwidth := termwidth / 5 * 3
	maxwidth := min(85, termwidth)
	indent := (termwidth - maxwidth) / 2

	context := &Context{scroll.DefaultClient, gemini.DefaultClient, nex.DefaultClient, []string{language}, nil, Certificate{}, true, ring.New(10), "scroll://scrollprotocol.us.to/" /*80*/, maxwidth, indent}
	context.scroll_client.NoTimeCheck = true
	context.gemini_client.NoTimeCheck = true

	/*
		var err error
		context.screen, err = tcell.NewScreen()
		if err != nil {
			fmt.Printf("Error: Failed to create screen.\n")
			os.Exit(1)
		}

		if err := context.screen.Init(); err != nil {
			fmt.Printf("Error: Failed to initialize screen.\n")
			os.Exit(1)
		}

		defStyle := tcell.StyleDefault.Background(tcell.ColorReset).Foreground(tcell.ColorReset)
		context.screen.SetStyle(defStyle)
		context.screen.Clear()
	*/

	cmd := ""
outer:
	for {
		fmt.Print(color.GreenString("scroll-term> "))
		var err error
		cmd, err = reader.ReadString('\n')
		if err != nil {
			continue
		}

		// Recalculate terminal size info
		termwidth, _, err := term.GetSize(int(os.Stdout.Fd()))
		if err != nil {
			//termwidth = 80 * 3
		}
		//context.maxwidth = termwidth / 5 * 3
		context.maxwidth = min(85, termwidth)
		context.indent = (termwidth - context.maxwidth) / 2

		cmd, args_str, _ := strings.Cut(strings.TrimSpace(cmd), " ")
		cmd = strings.ToLower(cmd)

		// Special cd alias handling
		if cmd == "cd" {
			if args_str == ".." {
				cmd = "up"
				args_str = ""
			} else if args_str == "." {
				cmd = "refresh"
				args_str = ""
			} else if args_str == "/" {
				cmd = "root"
				args_str = ""
			} else {
				cmd = "go"
			}
		}

		switch cmd {
		case "help":
			fmt.Printf("home - goes to configured homepage\n")
			fmt.Printf("go URL/int - goes to URL or link #\n")
			fmt.Printf("download URL/int - downloads resource at URL or link #\n")
			fmt.Printf("abstract URL/int - prints the abstract metadata of the URL or link #\n")
			fmt.Printf("history - goes to the history page\n")
			fmt.Printf("\n")
			fmt.Printf("refresh - refreshes the current page/resource\n")
			fmt.Printf("print - prints the current page again, but doesn't refresh\n")
			fmt.Printf("links - prints all the links of the current page\n")
			fmt.Printf("outline - prints all the headings of the current page\n")
			fmt.Printf("\n")
			fmt.Printf("url - print url of current page/resource\n")
			fmt.Printf("back - goes backwards in history\n")
			fmt.Printf("forward - goes forwards in history\n")
			fmt.Printf("up - goes up a directory\n")
			fmt.Printf("root - goes to root page\n")
			fmt.Printf("version - print current version of client software\n")
			fmt.Printf("exit - quits program\n")
		case "exit", "quit":
			break outer
		case "home", "homepage":
			GetAndPrintPage(context, context.homepage, false)
		case "cert": // Load a certificate
			cert_path, err := filepath.Abs(args_str)
			if err != nil {
				fmt.Printf("Error: Couldn't read argument as filepath.\n")
				continue
			}
			context.currentLoadedCert, err = os.ReadFile(cert_path)
			if err != nil {
				fmt.Printf("Error: Couldn't load certificate; %s.\n", err.Error())
				continue
			}
		case "download", "save": // TODO: Allow downloading/saving current page/resource
			if args_str == "" {
				// DownloadCurrentPageInCache
			} else {
				firstRune, _ := utf8.DecodeRuneInString(args_str)
				if unicode.IsDigit(firstRune) {
					integer, err := strconv.Atoi(args_str)
					if err == nil {
						cache := context.pageCache.Prev().Value.(PageCache)
						link := cache.Links[integer]
						DownloadPage(context, link.Url, false)
						continue
					}
				}
				DownloadPage(context, args_str, false)
			}
		case "refresh", "reload":
			context.pageCache = context.pageCache.Prev()
			pageCache, ok := context.pageCache.Value.(PageCache)
			if !ok {
				context.pageCache = context.pageCache.Next()
				continue outer
			} else if pageCache.PageType != PageType_URL {
				context.pageCache = context.pageCache.Next()
				PrintCurrentPageInCache(context)
			} else {
				GetAndPrintPage(context, context.currentURL.String(), false)
			}
		case "url", "cwd", "location":
			fmt.Printf("%s\n", context.currentURL.String())
		case "metadata", "abstract":
			firstRune, _ := utf8.DecodeRuneInString(args_str)
			if unicode.IsDigit(firstRune) {
				integer, err := strconv.Atoi(args_str)
				if err == nil {
					cache := context.pageCache.Prev().Value.(PageCache)
					link := cache.Links[integer]
					GetAndPrintPage(context, link.Url, true)
					continue
				}
			}

			if args_str == "" && context.currentURL != nil {
				// TODO: First try to get abstract in cache if it exists
				GetAndPrintPage(context, context.currentURL.String(), true)
			} else {
				GetAndPrintPage(context, args_str, true)
			}
		case "print":
			PrintCurrentPageInCache(context)
		case "", "links":
			PrintCurrentPageLinks(context)
		case "outline", "headings":
			PrintCurrentPageOutline(context)
		case "go", "goto", "navigate", "get":
			firstRune, _ := utf8.DecodeRuneInString(args_str)
			if unicode.IsDigit(firstRune) {
				integer, err := strconv.Atoi(args_str)
				if err == nil {
					cache := context.pageCache.Prev().Value.(PageCache)
					link := cache.Links[integer]
					GetAndPrintPage(context, link.Url, false)
					continue
				}
			}
			GetAndPrintPage(context, args_str, false)
		case "back", "b":
			context.pageCache = context.pageCache.Prev()
			if context.pageCache.Value != nil {
				pageCache := context.pageCache.Value.(PageCache)
				pageCache.InForwards = true
				context.pageCache.Value = pageCache
			}
			if context.pageCache.Prev().Value != nil {
				context.currentURL = context.pageCache.Prev().Value.(PageCache).url
				PrintCurrentPageInCache(context)
			}
		case "forward", "f":
			if context.pageCache.Value != nil {
				pageCache := context.pageCache.Value.(PageCache)
				if !pageCache.InForwards {
					continue outer
				} else {
					context.currentURL = pageCache.url
					pageCache.InForwards = false
					context.pageCache.Value = pageCache
				}
			}

			context.pageCache = context.pageCache.Next()
			PrintCurrentPageInCache(context)
		case "history":
			// Goes to History page
			GotoInternalPage(context, PageType_History)
			PrintCurrentPageInCache(context)
			context.currentURL = nil
		case "bookmarks", "library":
			GotoInternalPage(context, PageType_Bookmarks)
			PrintCurrentPageInCache(context)
			context.currentURL = nil
		case "up", "..":
			if context.currentURL != nil {
				newURL, err := context.currentURL.Parse("..")
				if err != nil {
					fmt.Printf("Error: Failed to go up a directory.\n")
					continue outer
				}
				GetAndPrintPage(context, newURL.String(), false)
			}
		case "root":
			if context.currentURL != nil {
				newURL, err := context.currentURL.Parse("/")
				if err != nil {
					fmt.Printf("Error: Failed to go up a directory.\n")
					continue outer
				}
				GetAndPrintPage(context, newURL.String(), false)
			}
		case "version":
			fmt.Printf("scroll-term %s\n", Version)
		case "brief":
			// Will print the links and headings, but nothing else.
			// TODO
		}

		firstRune, _ := utf8.DecodeRuneInString(cmd)
		if unicode.IsDigit(firstRune) {
			integer, err := strconv.Atoi(cmd)
			if err == nil {
				cache_ptr := context.pageCache.Prev()
				cache := cache_ptr.Value.(PageCache)
				if cache.PageType == PageType_URL {
					// Go to link from Links of current page.
					link := cache.Links[integer]
					GetAndPrintPage(context, link.Url, false)
				} else if cache.PageType == PageType_History {
					// Get link from History and go to it.
					index := 0
					context.pageCache.Do(func(a any) {
						switch a := a.(type) {
						case PageCache:
							if a.InForwards {
								return
							}
							if index == integer {
								if a.PageType == PageType_History || a.PageType == PageType_Bookmarks {
									GotoInternalPage(context, a.PageType)
									PrintCurrentPageInCache(context)
									context.currentURL = nil
								} else {
									GetAndPrintPage(context, a.url.String(), false)
								}
							}
						case nil:
							return
						}
						index++
					})
				} else if cache.PageType == PageType_Bookmarks {
					// TODO: Get link from Bookmarks and go to it.
				}
			}
		}

		fmt.Printf("\n")
	}
}

func GotoInternalPage(context *Context, pageType PageType) {
	switch pageType {
	case PageType_History:
		context.pageCache.Value = PageCache{false, PageType_History, nil, "", nil, "", time.Time{}, time.Time{}, "History", "", nil, nil, nil}
		context.pageCache = context.pageCache.Next()
	case PageType_Bookmarks:
		context.pageCache.Value = PageCache{false, PageType_Bookmarks, nil, "", nil, "", time.Time{}, time.Time{}, "Bookmarks", "", nil, nil, nil}
		context.pageCache = context.pageCache.Next()
	}
}

// Returns whether it printed the page
func PrintCurrentPageInCache(context *Context) bool {
	// Get current page from cache
	cache_ptr := context.pageCache.Prev()
	cache := cache_ptr.Value
	if cache == nil {
		return false
	}
	pageCache, ok := cache.(PageCache)
	if !ok || pageCache.InForwards {
		return false
	}

	if pageCache.PageType == PageType_History {
		// Print a list of links from the history
		index := 0
		context.pageCache.Do(func(a any) {
			switch a := a.(type) {
			case PageCache:
				if a.InForwards {
					return
				}
				if a.PageType == PageType_History || a.PageType == PageType_Bookmarks {
					fmt.Print(color.GreenString("[%d]", index))
					fmt.Printf(" %s\n", a.Title)
				} else {
					if context.currentURL.Scheme != "" && context.currentURL.Scheme != a.url.Scheme {
						fmt.Print(color.YellowString("[%d]", index))
					} else if a.url.Scheme == "http" || a.url.Scheme == "https" {
						fmt.Print(color.RedString("[%d]", index))
					} else {
						fmt.Print(color.CyanString("[%d]", index))
					}
					fmt.Printf(" %s", a.Title)
					if a.url.RawQuery != "" {
						query, _ := url.QueryUnescape(a.url.RawQuery)
						fmt.Print(" (Query: '")
						for i, r := range query {
							if i == 20 {
								break
							}
							fmt.Print(string(r))
						}
						if 10 < len(query) {
							fmt.Print("...")
						}
						fmt.Print("')")
					}
					fmt.Printf("\n")
				}
			case nil:
				return
			}
			index++
		})
	} else if pageCache.PageType == PageType_Bookmarks {
	} else if pageCache.PageType == PageType_URL {
		if pageCache.Mediatype == "text/scroll" || pageCache.Mediatype == "text/gemini" || pageCache.Mediatype == "text/markdown" {
			// Print Metadata
			indentString := strings.Repeat(" ", context.indent)
			if pageCache.Author != "" {
				fmt.Printf("%sAuthor: %s\n", indentString, pageCache.Author)
			}
			if pageCache.PublishDate != (time.Time{}) {
				fmt.Printf("%sPublish Date: %s\n", indentString, pageCache.PublishDate.Local().Format(DateTime_Kitchen))
			}
			if pageCache.ModificationDate != (time.Time{}) {
				fmt.Printf("%sModification Date: %s\n", indentString, pageCache.ModificationDate.Local().Format(DateTime_Kitchen))
			}
			if lang, hasLang := pageCache.Params["lang"]; hasLang {
				fmt.Printf("%sLanguage: %s\n", indentString, lang)
			}
			fmt.Printf("\n")

			// Print data
			links := []Link{}
			PrintGemini(context, pageCache.url, strings.NewReader(string(pageCache.PageData)), nil, &links, nil, context.indent)
		} else if pageCache.Mediatype == "text/nex" {
			// Print data
			links := []Link{}
			PrintNex(context, pageCache.url, strings.NewReader(string(pageCache.PageData)), &links, nil, context.indent, false)
		} else if strings.HasPrefix(pageCache.Mediatype, "text/") {
			PrintText(context, strings.NewReader(string(pageCache.PageData)), nil, context.indent, false)
		}
	}

	return true
}

func PrintCurrentPageLinks(context *Context) {
	cache := context.pageCache.Prev().Value
	if cache == nil {
		return
	}
	pageCache, ok := cache.(PageCache)
	if !ok || pageCache.InForwards {
		return
	}

	if pageCache.Mediatype == "text/scroll" || pageCache.Mediatype == "text/gemini" || pageCache.Mediatype == "text/markdown" {
		for _, link := range pageCache.Links {
			if link.SpartanInput {
				if link.WebLink {
					fmt.Print(color.RedString("[%d Input]", link.Id))
				} else if link.CrossHost {
					fmt.Print(color.YellowString("[%d Input]", link.Id))
				} else {
					fmt.Print(color.CyanString("[%d Input]", link.Id))
				}
			} else {
				if link.WebLink {
					fmt.Print(color.RedString("[%d]", link.Id))
				} else if link.CrossHost {
					fmt.Print(color.YellowString("[%d]", link.Id))
				} else {
					fmt.Print(color.CyanString("[%d]", link.Id))
				}
			}
			if link.Text != "" {
				fmt.Printf(" %s\n", link.Text)
			} else {
				fmt.Printf(" %s\n", link.Url)
			}
		}
	} else if pageCache.Mediatype == "text/nex" {
		for _, link := range pageCache.Links {
			if link.SpartanInput {
				if link.WebLink {
					fmt.Print(color.RedString("[%d Input]", link.Id))
				} else if link.CrossHost {
					fmt.Print(color.YellowString("[%d Input]", link.Id))
				} else {
					fmt.Print(color.CyanString("[%d Input]", link.Id))
				}
			} else {
				if link.WebLink {
					fmt.Print(color.RedString("[%d]", link.Id))
				} else if link.CrossHost {
					fmt.Print(color.YellowString("[%d]", link.Id))
				} else {
					fmt.Print(color.CyanString("[%d]", link.Id))
				}
			}
			if link.Text != "" {
				fmt.Printf(" => %s %s\n", link.Url, link.Text)
			} else {
				fmt.Printf(" => %s\n", link.Url)
			}
		}
	} else {
		fmt.Printf("Error: Unsupported mimetype.\n")
	}
}

func PrintCurrentPageOutline(context *Context) {
	cache := context.pageCache.Prev().Value
	if cache == nil {
		return
	}
	pageCache, ok := cache.(PageCache)
	if !ok || pageCache.InForwards {
		return
	}

	wordWrapper_heading1 := wordwrap.Wrapper(context.maxwidth, false)
	wordWrapper_heading2 := wordwrap.Wrapper(context.maxwidth-2, false)
	wordWrapper_heading3 := wordwrap.Wrapper(context.maxwidth-4, false)
	wordWrapper_heading4 := wordwrap.Wrapper(context.maxwidth-6, false)
	indentationString := strings.Repeat(" ", context.indent)

	if pageCache.Mediatype == "text/scroll" || pageCache.Mediatype == "text/gemini" || pageCache.Mediatype == "text/markdown" {
		for _, heading := range pageCache.Headings {
			switch heading.Level {
			case 1:
				multiline := wordWrapper_heading1(heading.Text)
				lines := strings.Split(multiline, "\n")
				for i, line := range lines {
					if i == 0 {
						color.Cyan("%s%s\n", indentationString, line)
					} else {
						color.Cyan("%s%s\n", indentationString, line)
					}
				}
			case 2:
				multiline := wordWrapper_heading2(heading.Text)
				lines := strings.Split(multiline, "\n")
				for i, line := range lines {
					if i == 0 {
						color.Cyan("%s  %s\n", indentationString, line)
					} else {
						color.Cyan("%s  %s\n", indentationString, line)
					}
				}
			case 3:
				multiline := wordWrapper_heading3(heading.Text)
				lines := strings.Split(multiline, "\n")
				for i, line := range lines {
					if i == 0 {
						color.Cyan("%s    %s\n", indentationString, line)
					} else {
						color.Cyan("%s    %s\n", indentationString, line)
					}
				}
			case 4:
				multiline := wordWrapper_heading4(heading.Text)
				lines := strings.Split(multiline, "\n")
				for i, line := range lines {
					if i == 0 {
						color.Cyan("%s      %s\n", indentationString, line)
					} else {
						color.Cyan("%s      %s\n", indentationString, line)
					}
				}
			}
		}
	} else if pageCache.Mediatype == "text/nex" {
		fmt.Printf("Error: Nex Listing documents don't have outlines.\n")
	} else {
		fmt.Printf("Error: Unsupported mimetype.\n")
	}
}

func DownloadPage(context *Context, url_str string, abstract bool) {
	URL, err := url.Parse(url_str)
	if err != nil {
		fmt.Printf("Error: Couldn't read URL.\n\n")
		return
	}

	// If no scheme, match it to the Scheme of the previous page
	if URL.Scheme == "" {
		URL.Scheme = context.currentURL.Scheme
	}

	//currentURL = args_str
	if URL.Scheme == "scroll" {
		HandleScrollProtocol(context, URL, context.languages, abstract, true, nil)
	} else if URL.Scheme == "gemini" {
		HandleGeminiProtocol(context, URL, true, nil)
	} else if URL.Scheme == "nex" {
		HandleNexProtocol(context, URL, true, nil, strings.HasSuffix(url_str, "/"))
	} else if URL.Scheme == "http" || URL.Scheme == "https" {
		browser.OpenURL(url_str)
	} else if URL.Scheme != "" {
		fmt.Printf("Error: Scheme not supported.\n\n")
		return
	}
}

func GetAndPrintPage(context *Context, url_str string, abstract bool) {
	URL, err := url.Parse(url_str)
	if err != nil {
		fmt.Printf("Error: Couldn't read URL.\n\n")
		return
	}

	// If no scheme, match it to the Scheme of the previous page
	if URL.Scheme == "" {
		URL.Scheme = context.currentURL.Scheme
	}

	var pageData bytes.Buffer
	if URL.Scheme == "scroll" {
		if !abstract {
			context.currentURL = URL
		}
		HandleScrollProtocol(context, URL, context.languages, abstract, false, &pageData)
	} else if URL.Scheme == "gemini" {
		if !abstract {
			context.currentURL = URL
		} else {
			fmt.Printf("Error: Abstracts not supported in Gemini Protocol.\n")
			return
		}
		HandleGeminiProtocol(context, URL, false, &pageData)
	} else if URL.Scheme == "nex" {
		if !abstract {
			context.currentURL = URL
		} else {
			fmt.Printf("Error: Abstracts not supported in Nex Protocol.\n")
			return
		}
		HandleNexProtocol(context, URL, false, &pageData, strings.HasSuffix(url_str, "/"))
	} else if URL.Scheme == "http" || URL.Scheme == "https" {
		if abstract {
			fmt.Printf("Error: Abstracts not supported.\n")
			return
		}
		browser.OpenURL(url_str)
	} else if URL.Scheme != "" {
		fmt.Printf("Error: Scheme not supported.\n\n")
		return
	}
}

func HandleScrollProtocol(context *Context, URL *url.URL, languages []string, abstract bool, download bool, pageData *bytes.Buffer) {
	reader := bufio.NewReader(os.Stdin)
	redirectTries := 0
	maxRedirectTries := 5
	var resp *scroll.Response
	for {
		var err error
		if context.currentLoadedCert != nil {
			resp, err = context.scroll_client.FetchWithCert(URL.String(), languages, abstract, context.currentLoadedCert, context.currentLoadedCert)
		} else {
			resp, err = context.scroll_client.Fetch(URL.String(), languages, abstract)
		}
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			return
		} else if resp.Status >= 40 && resp.Status <= 62 {
			fmt.Printf("Status: %d %s\n", resp.Status, scroll.StatusText(resp.Status))
			resp.Body.Close()
			return
		} else if resp.Status > 62 {
			fmt.Printf("Status: %d Unknown Status.\n", resp.Status)
			resp.Body.Close()
			return
		} else if resp.Status >= 30 && resp.Status <= 39 {
			if redirectTries < maxRedirectTries {
				// Redirect
				URL, err = URL.Parse(resp.Description)
				if err != nil {
					fmt.Printf("Error: Failed to parse redirect URL.\n")
					resp.Body.Close()
					return
				}
				resp.Body.Close()
				redirectTries++
			} else {
				fmt.Printf("Error: Exceeded maximum redirects.\n")
				resp.Body.Close()
				return
			}
		} else if resp.Status == 10 { // Input
			fmt.Printf("%s", resp.Description)
			if !strings.HasSuffix(resp.Description, " ") {
				fmt.Printf(" ")
			}
			resp.Body.Close()
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Printf("Error: Failed to read input.\n")
				return
			}
			input = strings.TrimSuffix(strings.ReplaceAll(input, "\r", ""), "\n")
			URL.RawQuery = url.QueryEscape(input)
		} else if resp.Status == 11 { // TODO: Sensitive Input
			fmt.Printf("%s", resp.Description)
			if !strings.HasSuffix(resp.Description, " ") {
				fmt.Printf(" ")
			}
			resp.Body.Close()
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Printf("Error: Failed to read input.\n")
				return
			}
			input = strings.TrimSuffix(strings.ReplaceAll(input, "\r", ""), "\n")
			URL.RawQuery = url.QueryEscape(input)
		} else {
			break
		}
	}

	// Check mimetype
	mediatype, params, err := mime.ParseMediaType(resp.Description)
	if err != nil {
		fmt.Printf("Error: Couldn't parse mimetype.\n\n")
		return
	}

	// TODO: Detect charset if it's unspecified in params

	// Print Metadata
	indentString := strings.Repeat(" ", context.indent)
	if resp.Author != "" {
		fmt.Printf("%sAuthor: %s\n", indentString, resp.Author)
	}
	if resp.PublishDate != (time.Time{}) {
		fmt.Printf("%sPublish Date: %s\n", indentString, resp.PublishDate.Local().Format(DateTime_Kitchen))
	}
	if resp.ModificationDate != (time.Time{}) {
		fmt.Printf("%sModification Date: %s\n", indentString, resp.ModificationDate.Local().Format(DateTime_Kitchen))
	}
	if lang, hasLang := params["lang"]; hasLang {
		fmt.Printf("%sLanguage: %s\n", indentString, lang)
	}
	if abstract {
		// If abstract, then print the returned mimetype as the contenttype of the file.
		fmt.Printf("%sContent-Type: %s\n", indentString, mediatype)
	}
	fmt.Printf("\n")

	// If scroll, gemtext, or markdown file, print the file out
	if !download && (abstract || mediatype == "text/markdown" || mediatype == "text/scroll" || mediatype == "text/gemini") {
		/*_, err := io.Copy(os.Stdout, resp.Body)
		if err != nil {
		}*/
		headings := []Heading{}
		links := []Link{}
		title, _ := PrintGemini(context, URL, resp.Body, &headings, &links, pageData, context.indent)
		if !abstract {
			context.pageCache.Value = PageCache{false, PageType_URL, URL, mediatype, params, resp.Author, resp.PublishDate, resp.ModificationDate, title, "", pageData.Bytes(), headings, links}
			context.pageCache = context.pageCache.Next()
		}
		resp.Body.Close()
	} else if !download && mediatype == "text/nex" {
		links := []Link{}
		PrintNex(context, URL, resp.Body, &links, pageData, context.indent, false)

		if !abstract {
			context.pageCache.Value = PageCache{false, PageType_URL, URL, mediatype, params, "", time.Time{}, time.Time{}, "", "", pageData.Bytes(), nil, links}
			context.pageCache = context.pageCache.Next()
		}
		resp.Body.Close()
	} else if !download && (strings.HasPrefix(mediatype, "text/")) {
		PrintText(context, resp.Body, pageData, context.indent, false)
		if !abstract {
			context.pageCache.Value = PageCache{false, PageType_URL, URL, mediatype, params, resp.Author, resp.PublishDate, resp.ModificationDate, "", "", pageData.Bytes(), nil, nil}
			context.pageCache = context.pageCache.Next()
		}
		resp.Body.Close()
	} else if !download && (mediatype == "audio/mpeg" || mediatype == "audio/mp3" || mediatype == "audio/x-mpeg" || mediatype == "audio/ogg" || mediatype == "audio/x-ogg" || mediatype == "application/ogg" || mediatype == "application/x-ogg" || mediatype == "audio/flac" || mediatype == "audio/x-flac" || mediatype == "audio/wav" || mediatype == "audio/x-wav" || mediatype == "audio/vnd.wave") {
		resp.SetReadTimeout(0)
		StreamAudio(mediatype, params, resp.Body)
	} else if !download && (context.supportMPV && (strings.HasPrefix(mediatype, "audio/") || strings.HasPrefix(mediatype, "video/"))) {
		resp.SetReadTimeout(0)
		StreamVideo(mediatype, params, resp.Body)
		resp.Body.Close()
	} else {
		DownloadFile(resp.Body, URL)
		resp.Body.Close()
	}
}

func HandleGeminiProtocol(context *Context, URL *url.URL, download bool, pageData *bytes.Buffer) {
	reader := bufio.NewReader(os.Stdin)
	redirectTries := 0
	maxRedirectTries := 5
	var resp *gemini.Response
	for {
		var err error
		if context.currentLoadedCert != nil {
			resp, err = context.gemini_client.FetchWithCert(URL.String(), context.currentLoadedCert, context.currentLoadedCert)
		} else {
			resp, err = context.gemini_client.Fetch(URL.String())
		}
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			return
		} else if resp.Status >= 40 && resp.Status <= 62 {
			fmt.Printf("Status: %s\n", scroll.StatusText(resp.Status))
			resp.Body.Close()
			return
		} else if resp.Status > 62 {
			fmt.Printf("Status: %d Unknown Status.\n", resp.Status)
			resp.Body.Close()
			return
		} else if resp.Status >= 30 && resp.Status <= 39 {
			if redirectTries < maxRedirectTries {
				// Redirect
				URL, err = URL.Parse(resp.Meta)
				if err != nil {
					fmt.Printf("Error: Failed to parse redirect URL.\n")
					resp.Body.Close()
					return
				}
				resp.Body.Close()
			} else {
				fmt.Printf("Error: Exceeded maximum redirects.\n")
				resp.Body.Close()
				return
			}
		} else if resp.Status == 10 { // Input
			fmt.Printf("%s", resp.Meta)
			if !strings.HasSuffix(resp.Meta, " ") {
				fmt.Printf(" ")
			}
			resp.Body.Close()
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Printf("Error: Failed to read input.\n")
				return
			}
			input = strings.TrimSuffix(strings.ReplaceAll(input, "\r", ""), "\n")
			URL.RawQuery = url.QueryEscape(input)
		} else if resp.Status == 11 { // TODO: Sensitive Input
			fmt.Printf("%s", resp.Meta)
			if !strings.HasSuffix(resp.Meta, " ") {
				fmt.Printf(" ")
			}
			resp.Body.Close()
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Printf("Error: Failed to read input.\n")
				return
			}
			input = strings.TrimSuffix(strings.ReplaceAll(input, "\r", ""), "\n")
			URL.RawQuery = url.QueryEscape(input)
		} else {
			break
		}
	}

	// Check mimetype
	mediatype, params, err := mime.ParseMediaType(resp.Meta)
	if err != nil {
		fmt.Printf("Error: Couldn't parse mimetype.\n\n")
		resp.Body.Close()
		return
	}

	// Print Metadata
	indentString := strings.Repeat(" ", context.indent)
	if lang, hasLang := params["lang"]; hasLang {
		fmt.Printf("%sLanguage: %s\n", indentString, lang)
	}
	fmt.Printf("\n")

	// If scroll, gemtext, or markdown file, print the file out
	if !download && (mediatype == "text/markdown" || mediatype == "text/scroll" || mediatype == "text/gemini") {
		/*_, err := io.Copy(os.Stdout, resp.Body)
		if err != nil {
		}*/
		headings := []Heading{}
		links := []Link{}
		title, _ := PrintGemini(context, URL, resp.Body, &headings, &links, pageData, context.indent)

		context.pageCache.Value = PageCache{false, PageType_URL, URL, mediatype, params, "", time.Time{}, time.Time{}, title, "", pageData.Bytes(), headings, links}
		context.pageCache = context.pageCache.Next()
		resp.Body.Close()
	} else if !download && mediatype == "text/nex" {
		links := []Link{}
		PrintNex(context, URL, resp.Body, &links, pageData, context.indent, false)

		context.pageCache.Value = PageCache{false, PageType_URL, URL, mediatype, params, "", time.Time{}, time.Time{}, "", "", pageData.Bytes(), nil, links}
		context.pageCache = context.pageCache.Next()
		resp.Body.Close()
	} else if !download && strings.HasPrefix(mediatype, "text/") {
		PrintText(context, resp.Body, pageData, context.indent, false)
		context.pageCache.Value = PageCache{false, PageType_URL, URL, mediatype, params, "", time.Time{}, time.Time{}, "", "", pageData.Bytes(), nil, nil}
		context.pageCache = context.pageCache.Next()
		resp.Body.Close()
	} else if !download && (mediatype == "audio/mpeg" || mediatype == "audio/x-mpeg" || mediatype == "audio/mp3" || mediatype == "audio/ogg" || mediatype == "audio/x-ogg" || mediatype == "application/ogg" || mediatype == "application/x-ogg" || mediatype == "audio/flac" || mediatype == "audio/x-flac" || mediatype == "audio/wav" || mediatype == "audio/wave" || mediatype == "audio/x-wav" || mediatype == "audio/vnd.wave") {
		resp.SetReadTimeout(0)
		StreamAudio(mediatype, params, resp.Body)
	} else if !download && (context.supportMPV && (strings.HasPrefix(mediatype, "audio/") || strings.HasPrefix(mediatype, "video/"))) {
		resp.SetReadTimeout(0)
		StreamVideo(mediatype, params, resp.Body)
		resp.Body.Close()
	} else {
		DownloadFile(resp.Body, URL)
		resp.Body.Close()
	}
}

func HandleNexProtocol(context *Context, URL *url.URL, download bool, pageData *bytes.Buffer, directory bool) {
	conn, err := context.nex_client.Request(URL.String())
	if err != nil {
		fmt.Printf("Error: Failed; %s\n", err.Error())
	}
	if directory || path.Ext(URL.Path) == ".nex" || (path.Ext(URL.Path) == "" && path.Base(URL.Path) == "index") {
		links := []Link{}
		PrintNex(context, URL, conn, &links, pageData, context.indent, false)

		context.pageCache.Value = PageCache{false, PageType_URL, URL, "text/nex", map[string]string{}, "", time.Time{}, time.Time{}, "", "", pageData.Bytes(), nil, links}
		context.pageCache = context.pageCache.Next()
		conn.Close()
	} else {
		detectionData, err := io.ReadAll(io.LimitReader(conn, 3072)) // Read 3072 bytes to try to detect mimetype
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			conn.Close()
			return
		}
		mt, err := mimetype.DetectReader(bytes.NewReader(detectionData))
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			conn.Close()
			return
		}
		multiReader := io.MultiReader(bytes.NewReader(detectionData), conn) // Concatenate new data onto already read data

		// If couldn't detect mimetype based on file data, try using the extension instead
		if mt.Is("application/octet-stream") {
			mt = mimetype.Lookup(mime.TypeByExtension(path.Ext(URL.Path)))
		}

		if !download && (path.Ext(URL.Path) == ".gmi" || path.Ext(URL.Path) == ".gemini") {
			headings := []Heading{}
			links := []Link{}
			title, _ := PrintGemini(context, URL, multiReader, &headings, &links, pageData, context.indent)

			context.pageCache.Value = PageCache{false, PageType_URL, URL, "text/gemini", map[string]string{}, "", time.Time{}, time.Time{}, title, "", pageData.Bytes(), headings, links}
			context.pageCache = context.pageCache.Next()
			conn.Close()
		} else if !download && (path.Ext(URL.Path) == ".scroll") {
			headings := []Heading{}
			links := []Link{}
			title, _ := PrintGemini(context, URL, multiReader, &headings, &links, pageData, context.indent)

			context.pageCache.Value = PageCache{false, PageType_URL, URL, "text/scroll", map[string]string{}, "", time.Time{}, time.Time{}, title, "", pageData.Bytes(), headings, links}
			context.pageCache = context.pageCache.Next()
			conn.Close()
		} else if !download && (path.Ext(URL.Path) == ".md" || path.Ext(URL.Path) == ".markdown") {
			headings := []Heading{}
			links := []Link{}
			title, _ := PrintGemini(context, URL, multiReader, &headings, &links, pageData, context.indent)

			context.pageCache.Value = PageCache{false, PageType_URL, URL, "text/markdown", map[string]string{}, "", time.Time{}, time.Time{}, title, "", pageData.Bytes(), headings, links}
			context.pageCache = context.pageCache.Next()
			conn.Close()
		} else if !download && strings.HasPrefix(mt.String(), "text/") {
			PrintText(context, multiReader, pageData, context.indent, false)
			context.pageCache.Value = PageCache{false, PageType_URL, URL, mt.String(), map[string]string{}, "", time.Time{}, time.Time{}, "", "", pageData.Bytes(), nil, nil}
			context.pageCache = context.pageCache.Next()
			conn.Close()
		} else if !download && (mimetype.EqualsAny(mt.String(), "audio/x-mpeg", "audio/mpeg", "audio/ogg", "audio/x-ogg", "application/ogg", "application/x-ogg", "audio/flac", "audio/x-flac", "audio/wav", "audio/wave", "audio/x-wav", "audio/vnd.wav")) {
			conn.SetDeadline(time.Time{})
			StreamAudio(mt.String(), nil, io.NopCloser(multiReader))
			conn.Close() // Note: Close here because multiReader uses a NopCloser
		} else if !download && (context.supportMPV && (path.Ext(URL.Path) == ".mp4" || strings.HasPrefix(mt.String(), "audio/") || strings.HasPrefix(mt.String(), "video/"))) {
			conn.SetDeadline(time.Time{})
			StreamVideo(mt.String(), nil, multiReader)
			conn.Close()
		} else {
			DownloadFile(multiReader, URL)
			conn.Close()
		}
	}
}

func StreamAudio(mediatype string, params map[string]string, reader io.ReadCloser) {
	cacheDuration := time.Second / 5
	if mediatype == "audio/mpeg" || mediatype == "audio/mp3" || mediatype == "audio/x-mpeg" {
		stream, format, err := mp3.Decode(reader)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}

		speaker.Init(format.SampleRate, format.SampleRate.N(cacheDuration))
		speaker.PlayAndWait(stream)
		stream.Close()
	} else if mediatype == "audio/ogg" || mediatype == "audio/x-ogg" || mediatype == "application/ogg" || mediatype == "application/x-ogg" {
		// TODO: Use params["codecs"]?
		stream, format, err := vorbis.Decode(reader)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}

		speaker.Init(format.SampleRate, format.SampleRate.N(cacheDuration))
		speaker.PlayAndWait(stream)
		stream.Close()
	} else if mediatype == "audio/flac" || mediatype == "audio/x-flac" {
		stream, format, err := flac.Decode(reader)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}

		speaker.Init(format.SampleRate, format.SampleRate.N(cacheDuration))
		speaker.PlayAndWait(stream)
		stream.Close()
	} else if mediatype == "audio/wav" || mediatype == "audio/wave" || mediatype == "audio/x-wav" || mediatype == "audio/vnd.wave" {
		stream, format, err := wav.Decode(reader)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}

		speaker.Init(format.SampleRate, format.SampleRate.N(cacheDuration))
		speaker.PlayAndWait(stream)
		stream.Close()
	}
}

func StreamVideo(mediatype string, params map[string]string, reader io.Reader) {
	cmd := exec.Command("mpv", "-", "--demuxer-lavf-probesize=32000")
	stdin, err := cmd.StdinPipe()
	if err != nil {
		fmt.Printf("Error: Cannot open pipe to mpv.\n")
	}

	err = cmd.Start()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}
	io.Copy(stdin, reader)
	fmt.Printf("Finished downloading video...\n")
	err = cmd.Wait()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}
}

func DownloadFile(reader io.Reader, URL *url.URL) {
	// Download the file
	newfile := filepath.Join("./downloads", path.Base(URL.Path))
	file, err := os.OpenFile(newfile, os.O_CREATE|os.O_SYNC, 0600)
	if err != nil {
		fmt.Printf("Error: Failed to create file.\n")
		return
	}
	defer file.Close()
	_, err = io.Copy(file, reader)
	if err != nil {
		fmt.Printf("Error: Failed to write to file.\n")
		return
	}
	file.Sync()
	fmt.Printf("File downloaded to %s.\n", newfile)
}
