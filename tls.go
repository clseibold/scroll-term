package main

import (
	"bytes"
	"crypto"
	crypto_rand "crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"math/big"
	"net"
	"os"
	"time"
)

// tlsconn wraps every necessary method from *tls.Conn, so it can be stubbed.
type tlsconn interface {
	net.Conn
	ConnectionState() tls.ConnectionState
}

var OID_USER_ID = asn1.ObjectIdentifier{0, 9, 2342, 19200300, 100, 1, 1}
var OID_CN = asn1.ObjectIdentifier{2, 5, 4, 3}

type CertInfo struct {
	blurb       string
	domains     []string
	mailbox     string
	mailAddress string
	fingerprint string
	IsCA        bool
}

// NOTE: Assumes that cert and private key are in a single (pem) file.
func getCertsFromPem(certFilename string) (*x509.Certificate, crypto.PrivateKey, []byte) {
	file, readErr := os.ReadFile(certFilename)
	if readErr != nil {
		panic(fmt.Errorf("could not read certificate file %q: %v", certFilename, readErr))
	}

	pemBlock, rest := pem.Decode(file)
	var certificate *x509.Certificate
	var privateKey crypto.PrivateKey
	var privateKeyPemBytes []byte
	for pemBlock != nil {
		var parseErr error
		if pemBlock.Type == "CERTIFICATE" {
			certificate, parseErr = x509.ParseCertificate(pemBlock.Bytes)
		} else if pemBlock.Type == "RSA PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "EC PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParseECPrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
		}
		if parseErr != nil {
			panic(parseErr)
		}
		pemBlock, rest = pem.Decode(rest)
	}

	return certificate, privateKey, privateKeyPemBytes
}

// NOTE: Assumes that cert and private key are in a single (pem) file.
func getCertsFromPemData(certData []byte) (*x509.Certificate, crypto.PrivateKey, []byte) {
	pemBlock, rest := pem.Decode(certData)
	var certificate *x509.Certificate
	var privateKey crypto.PrivateKey
	var privateKeyPemBytes []byte
	for pemBlock != nil {
		var parseErr error
		if pemBlock.Type == "CERTIFICATE" {
			certificate, parseErr = x509.ParseCertificate(pemBlock.Bytes)
		} else if pemBlock.Type == "RSA PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "EC PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParseECPrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
		}
		if parseErr != nil {
			panic(parseErr)
		}
		pemBlock, rest = pem.Decode(rest)
	}

	return certificate, privateKey, privateKeyPemBytes
}

// Returns Common Name (aka. blurb), mailbox, hostname(s), and mailAddress
func getCertInfo(certificate *x509.Certificate) CertInfo {
	commonName := certificate.Subject.CommonName // aka. blurb
	domains := certificate.DNSNames              // aka. hostname
	mailbox := ""
	for _, n := range certificate.Subject.Names {
		if n.Type.String() == "0.9.2342.19200300.100.1.1" {
			mailbox = n.Value.(string)
		}
	}
	mailAddress := mailbox + "@"
	if len(domains) > 0 {
		mailAddress = mailbox + "@" + domains[0]
	}

	h := sha256.New()
	h.Write(certificate.Raw)
	fingerprint := hex.EncodeToString(h.Sum(nil))

	return CertInfo{commonName, domains, mailbox, mailAddress, fingerprint, certificate.IsCA}
}

// Generate new self-signed certificate (with CA set to true)
func generateSelfSignedCertAndPrivateKey(common_name string, countries []string, organizations []string, organizationalUnits []string, hostnames []string, pemBuffer *bytes.Buffer) (*x509.Certificate, error) {
	// Generate random serial number for Cert
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := crypto_rand.Int(crypto_rand.Reader, serialNumberLimit)
	if err != nil {
		//cmd.PrintErrf("Error generating serial number for certificate: %s\n", err)
		return nil, err
	}

	// Create Server Certificate
	privateKey, err := rsa.GenerateKey(crypto_rand.Reader, 2048)
	if err != nil {
		//cmd.PrintErrf("Error generating RSA private key: %s\n", err)
		return nil, err
	}

	subject := pkix.Name{
		CommonName: common_name,
		//ExtraNames:         []pkix.AttributeTypeAndValue{{Type: OID_USER_ID, Value: string(cert_mailbox_name)}},
		Country:            countries,
		Organization:       organizations,
		OrganizationalUnit: organizationalUnits,
	}
	issuer := subject
	certificate := &x509.Certificate{
		IsCA:                  true,
		DNSNames:              hostnames,
		Subject:               subject,
		Issuer:                issuer,
		SerialNumber:          serialNumber,
		NotBefore:             time.Now().UTC(),
		NotAfter:              time.Now().UTC().Add(time.Hour * 24 * 365 * 200), // 200 years
		SignatureAlgorithm:    x509.SHA256WithRSA,
		BasicConstraintsValid: true,
	}
	var parent *x509.Certificate
	var signerPrivateKey any
	var publicKey any

	// Self-signed: Use itself as parent cert, and its own private key as the signer
	parent = certificate
	signerPrivateKey = privateKey
	publicKey = &privateKey.PublicKey

	certBytes_DER, createErr := x509.CreateCertificate(crypto_rand.Reader, certificate, parent, publicKey, signerPrivateKey) // NOTE: When making mailboxes, privateKey should be parent's private key!
	if createErr != nil {
		//cmd.PrintErrf("Error creating certificate: %s\n", createErr)
		return nil, err
	}

	finalCertificate, parseErr := x509.ParseCertificate(certBytes_DER)
	if parseErr != nil {
		return nil, err
	}

	// Convert private key to DER bytes
	privateKeyBytes_DER := x509.MarshalPKCS1PrivateKey(privateKey)

	// Convert cert to pem format and save it
	certBlock := pem.Block{Type: "CERTIFICATE", Bytes: certBytes_DER}
	privBlock := pem.Block{Type: "RSA PRIVATE KEY", Bytes: privateKeyBytes_DER}
	encodeErr := pem.Encode(pemBuffer, &certBlock)
	if encodeErr != nil {
		//cmd.PrintErrf("Could not encode certificate to PEM format.\n")
		return nil, err
	}
	encodeErr = pem.Encode(pemBuffer, &privBlock)
	if encodeErr != nil {
		//cmd.PrintErrf("Could not encode certificate to PEM format.\n")
		return nil, err
	}

	return finalCertificate, nil
}
