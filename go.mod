module gitlab.com/clseibold/scroll-term

go 1.21.9

require (
	github.com/clseibold/go-gemini v0.0.0-20240314051634-436d3e54df5c
	github.com/eidolon/wordwrap v0.0.0-20161011182207-e0f54129b8bb
	github.com/fatih/color v1.16.0
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/gopxl/beep v1.4.0
	github.com/pkg/browser v0.0.0-20240102092130-5ac0b6a4141c
	github.com/rivo/uniseg v0.4.7
	gitlab.com/clseibold/gonex v0.0.0-20231012064938-fedecb8758b2
	golang.org/x/net v0.22.0
	golang.org/x/term v0.18.0
)

require (
	github.com/ebitengine/oto/v3 v3.1.0 // indirect
	github.com/ebitengine/purego v0.5.0 // indirect
	github.com/hajimehoshi/go-mp3 v0.3.4 // indirect
	github.com/icza/bitio v1.1.0 // indirect
	github.com/jfreymuth/oggvorbis v1.0.5 // indirect
	github.com/jfreymuth/vorbis v1.0.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mewkiz/flac v1.0.8 // indirect
	github.com/mewkiz/pkg v0.0.0-20230226050401-4010bf0fec14 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
